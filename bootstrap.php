<?php
/**
 * ION Engine Config File
 * The main config file that initiates the bootstrap process
 *
 *
 * Main resource paths defined for global usage across ION Engine
 * The two abbreviations CB and CF represent the taxonomy
 * between Code-Behind and Code-Front
 */
error_reporting(0);

define('CB_ROOT',str_replace('\\','/',dirname(__FILE__)).'/');
define('CB_LIB',CB_ROOT.'vendor/');
define('CB_APPS',CB_ROOT.'apps/');
define('CB_COST',10);

/**
 * Front-end paths
 */
define('CF_DOMAIN',rtrim($_SERVER['HTTP_HOST'],'/'));
define('CF_PROTOCOL',($_SERVER['HTTPS'] !== null) ? "https://" : "http://");
define('CF_URL', CF_PROTOCOL.rtrim(CF_DOMAIN,'/').$_SERVER['REQUEST_URI']);
define('CF_ROOT',CF_PROTOCOL.CF_DOMAIN);
define('CF_APPS',CF_ROOT.'/apps/');

require CB_LIB.'autoload.php';