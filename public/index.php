<?php
include('../bootstrap.php');

$ion = burakg\ion\ion::get();

$ion
    ->register_all_apps()
	->front()
		->router()
			->execute($_SERVER['REQUEST_URI']);
