<!DOCTYPE html>
<html lang="{{curlang}}">
<head>
    <title>{{title}}</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    {{metatags}}
    {{ogmeta}}
    {{stylesheets}}{{extrastylesheets}}
</head>
<body class="{{bodyclass}}" data-pagejsmethod="{{pagejsmethod}}">
<header id="page-header">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{root}}">ION Engine</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    {{navigation}}
                </ul>
                <ul class="nav navbar-nav navbar-right">{{languagenav}}</ul>
            </div>
        </div>
    </nav>
</header>
<div class="container main-content">
    {{content}}
</div>

{{scripts}}{{extrascripts}}

<script type="text/javascript">
    var Site = {
        ROOT: '{{root}}',
        ABSROOT: '{{absroot}}',
        ASSETS: '{{assets}}',
        URL: '{{url}}',
        TITLE: "{{title}}",
        SITETITLE: "{{sitetitle}}"
    };
    ION.generic();
</script>
</body>
</html>