<?php
namespace ionApp\website;

use burakg\ion\front\asset;
use burakg\ion\front\auth;
use burakg\ion\front\masterpage;
use burakg\ion\front\navigation;
use burakg\ion\helpers;
use burakg\ion\ion;
use ionApp\ionAdmin\page;

class utilities
{
    /**
     * @param $url
     * @param bool $json_decode
     * @return apiResponse
     */
    public static function makeCall($url, $json_decode=true){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        curl_close($ch);

        return new apiResponse((curl_error($ch) == 0),curl_error($ch),null,($json_decode) ? json_decode($response) : $response);
    }

    /**
     * @param $name
     * @return array
     */
    public static function parseName($name){
        $fullName = explode(' ', $name);
        $last_name = end($fullName);
        $first_name = str_replace(' '.$last_name,null,$name);

        return ['first_name' => $first_name,'last_name' => $last_name, 'full_name' => $name];
    }

    public static function testToken(){
        auth::get();
        if (!empty($_POST['csrfToken'])) {
            if (hash_equals($_SESSION['csrf_token'], $_POST['csrfToken'])) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function setToken(){
        if(empty($_SESSION['csrf_token'])) {
            try {
                $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
            }catch (\Exception $e){}
        }
    }

    public static function getCanonicalHrefs()
    {
        $ion = ion::get();
        $currentNode = $ion->front()->sitemap()->get_current_node();
        $key = ($currentNode['root'] !== true) ? 'canon-page-'.$currentNode['id'].'-'.$ion->curLang : 'canon-page-root-'.$ion->curLang;

        $cache = new File($ion->get_current_app()->CB_ROOT . '_cache');
        $value = $cache->get($key);

        if(!isset($value) || $_GET['debug']){
            $app = $ion->get_current_app();
            $dataPath = $app->DATA_ROOT;
            $langs = $ion->language()->get_langs();

            $output = null;
            foreach($langs AS $l){
                $regions = (strlen($l->regions) > 0) ? explode(',',$l->regions) : [];

                if($currentNode['root']){
                    $output .= '<link rel="alternate" href="'.CF_ROOT.'/'.$l->short.'/" hreflang="'.$l->short.'" />';
                    foreach($regions AS $region){
                        $output .= '<link rel="alternate" href="'.CF_ROOT.'/'.$l->short.'/" hreflang="'.$region.'" />';
                    }
                }else{
                    $json = new JsonStore(json_decode(file_get_contents($dataPath.'sitemap_'.$l->short.'.json'),true));

                    $node = $json->get("$..node[?(@.id=={$currentNode['id']})]");

                    if($node[0]['id'] == $currentNode['id'] && $node[0]['title'] != ''){
                        $output .= '<link rel="alternate" href="'.CF_ROOT.'/'.$l->short.'/'.$node[0]['fullpath'].'" hreflang="'.$l->short.'" />';

                        foreach($regions AS $region){
                            $output .= '<link rel="alternate" href="'.CF_ROOT.'/'.$l->short.'/'.$node[0]['fullpath'].'" hreflang="'.$region.'" />';
                        }
                    }
                }
            }
            unset($json);
            unset($node);

            $cache->set($key,$output,3600);

            return $cache->get($key);
        }else{
            return $value;
        }
    }

    /**
     * Sets the masterpage template field values along with custom css and js file imports quickly
     *
     * @param array $templateFields
     * @param asset|null $asset
     */
    public static function quickSettings($templateFields, $asset=null)
    {
        masterpage::get()->set_callback(function() use($templateFields,$asset){
            $master = masterpage::get();

            if($asset !== null){
                $output = $asset->output();
                $master
                    ->apply_template_field('extrastylesheets',$output['css_screen'])
                    ->apply_template_field('extrascripts',$output['js']);
            }

            if(is_array($templateFields)){
                foreach($templateFields AS $fieldName => $value){
                    $master
                        ->apply_template_field($fieldName,$value);
                }
            }
        });
    }

    /**
     * Returns page details by id from the db
     *
     * @param string $id
     * @param bool $fetchImages
     * @return array
     */
    public static function pageData(string $id, bool $fetchImages = false): array
    {
        $data = new page;
        $pageDetails = $data->getTranslation(ion::get()->curLang, $id)->data;
        $pageDetails['customFields'] = $pageDetails['customFields'][ion::get()->curLang];
        $pageDetails['mainImage'] = $data->show_main_image([],[],null,false);

        if($fetchImages === true){
            $pageDetails['gallery'] = $data->get_images(
                false,
                [$data->resizeSizes['th'][0], $data->resizeSizes['th'][1]],
                false
            );
            if(!is_array($pageDetails['gallery'][0]))
                $pageDetails['gallery'] = [];
        }

        return $pageDetails;
    }

    /**
     * @param $src
     * @return string
     */
    public static function customDataImage($src): string
    {
        $img = helpers::get()->html()->show_image(strrchr($src,'/'),[],null,false);
        return $img !== null ? $img : '';
    }
}
