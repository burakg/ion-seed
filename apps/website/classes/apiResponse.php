<?php
namespace ionApp\website;

class apiResponse
{
    /**
     * @var integer
     */
    public $code;

    /**
     * @var string
     */
    public $message;

    /**
     * @var mixed
     */
    public $data;

    /**
     * @var boolean
     */
    public $status;

    /**
     * apiResponse constructor.
     * @param $status
     * @param $code
     * @param $message
     * @param null|mixed $data
     */
    public function __construct($status,$code,$message,$data=null)
    {
        $this->status = $status;
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value){}

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'status' => $this->status,
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data,
        ]);
    }
}