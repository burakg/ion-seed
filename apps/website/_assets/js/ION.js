let ION = {
    generic: function(){
        // Put your site-wide methods to run on each page.

        // Here are two generic methods you might need in your every day projects.
        $(".mailize").each(function(){
            let _this = $(this);
            _this.html(ION.email(_this.data('account'),_this.data('domain')));
        });
        let _method = $("body").data("pagejsmethod");
        if(typeof _method != "undefined" && typeof ION.pageMethods[_method] != "undefined"){
            ION.pageMethods[_method]();
        }
    },
    pageMethods: {
        // define your page-specific methods here.
        // like this:
        // homePage: function(){
        //      your procedures go here
        // }
    },
    footer: function(){
        let _dim = {
            wh: $(window).height(),
            ch: $(".main-content").outerHeight(),
            fh: $(".page-footer").outerHeight(),
            hh: $("#page-header").outerHeight()
        };

        if((_dim.ch + _dim.ch + _dim.fh) <= _dim.wh){
            let _val = (_dim.wh - _dim.ch - _dim.fh - _dim.hh);
            if(_val > 0){
                $(".main-content").css("margin-bottom", _val);
            }
        }else
            $(".main-content").css("margin-bottom",0);
    },
    email: function(account,domain){
        let _email = account + '@' + domain;
        let _text = '<a href="mailto:' + _email + '">' + _email + '</a>';
        return(_text);
    }
};
